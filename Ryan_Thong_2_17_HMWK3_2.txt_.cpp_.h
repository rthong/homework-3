//This code will output 1 4 1 0 0 5
#include <iostream>
using namespace std;
int main ()
{
    int numbers[50];
    int * p;
    
    p = numbers;
    *p = 1;
    
    p++;
    *p = 4;
    
    p = &numbers[2];
    *p = numbers[0];
    
    p = numbers + *p;
    *p = 4;
    
    p = numbers+1;
    *(p+4) = 5;
    cout<< (float)numbers[4] <<endl;
    for(int i = 0; i < 50; i++)
        cout<< numbers[i]<<endl;
    
//    cout << "Numbers : "<<numbers[0]
//                <<" "<<numbers[1]
//                <<" "<<numbers[2]
//                <<" "<<numbers[3]
//                <<" "<<numbers[4]
//                <<" "<<numbers[5]<<endl;
    
return 0;
}
//My prediction did not match what was outputted. I debugged the code by checking the fourth memory cup in the array because the output was not the same as my prediction. Then I came to the conclusion that the program I am running is outputting random numbers at memory cups that should be values of 0. 
