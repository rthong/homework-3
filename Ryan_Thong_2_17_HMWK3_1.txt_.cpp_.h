#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

int main(){
    
    vector<float> nums;
    float sum = 0;
    float RMS = 0;
    nums.push_back(34.2);
    nums.push_back(89.3);
    nums.push_back(1.2);
    nums.push_back(20.3);
    nums.push_back(1111.3);
    nums.push_back(9.3);
    
    for(int i=0; i<nums.size(); i++){
        sum += nums[i];
    }
    
    float mean = sum/nums.size();
    
    for(int i=0; i<nums.size(); i++){
        RMS += pow(nums[i],2);
    }
    RMS = RMS/nums.size();
    RMS = pow(RMS,.5);
    
    cout <<"The sum of the values are: " << sum <<endl;
    cout <<"The mean of the values are: " << mean <<endl;
    cout <<"The RMS of the values are: " << RMS <<endl;
    
    
    return 0;
}
